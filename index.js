
let num=2;
const getCube = num**3;
console.log(`The cube of ${num} is ${getCube}`);


const address = ["Belen St.", "Gulod", "Quezon City", 1117];
const [street, barangay, city, zipCode] = address;
console.log(`I live at ${street} ${barangay} ${city} ${zipCode}`);


const animal = {
	name: "Raffy",
	type: "giraffe",
	weight: "1900 Kilograms",
	measurement: "16 feet" 
}

const{name, type, weight, measurement} = animal;
console.log(`${name} is a ${type}. He weighed ${weight} with a measurement of ${measurement}.`);


const numbers = [1,2,3,4,5]

numbers.forEach((number)=> {
	console.log(number);
})



const reduceNumber = numbers.reduce((num, total) => num + total
);
console.log(reduceNumber);
